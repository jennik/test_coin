<?php
require_once 'src/autoloader.php';

$cmd = $argv[1] ?? 'none';

switch ($cmd) {
    case 'info':
        info();
        break;
    case 'create-wallet': 
        createWallet();
        break;
    case 'trust':
        $amount = $argv[3] ?? '';
        $wallet = $argv[2] ?? '';
        if (!$amount || !$wallet) {
            exit("You must specify amount and wallet!");
        }
        pay ($wallet, $amount);
        break;
    case 'calc':
        $from = $argv[2] ?? '';
        $to = $argv[3] ?? '';
        if (!$from || !$to) {
            exit("You must specify two arguments");
        }
        $visited = [];
        $res = trust($from, $to, $visited, 1);
        echo "Trust rate is: $res\n";
        break;
    default:
        exit("Unknown command!\n");
}

function createWallet()
{
    $keys = KeyPair::generateKeyPair(KEY_PATH);
    $wallet = $keys->getWallet();
    echo "Your wallet id is {$wallet}\n";
}


function pay($to, $amount)
{
    if ($amount > 100) {
        $amount = 100;
    }
    if ($amount < -100) {
        $amount = -100;
    }

    $repo = new Repository();
    $keys = KeyPair::getFromFiles(KEY_PATH);

    $transaction = Transaction::createNewWithKeys($to, $amount, $keys);
    $success = $repo->addNewTransaction($transaction);

    if ($success) {
        $gm = new GossipMonger($repo);
        $gm->tellAboutNewTransaction($transaction);
        echo "Transaction created\n";
    } else {
        exit("New transaction failed\n");
    }
}

function trust($from, $to, &$visited, $level)
{
    $trust = 0;

    $repo = new Repository();
    $trustMap = $repo->findAllTransactions();

    if (!isset($trustMap[$from])) {
        return 0;
    }

    foreach ($trustMap[$from] as $trustItem) {
        if (in_array($trustItem['id'], $visited)) {
            continue;
        } else {
            $visited[] = $trustItem['id'];
        }

        if ($trustItem['id'] == $to) {
            $trust += $trustItem['rate'];
        }
    }

    if ($trust) {
        return $trust;
    }

    foreach ($trustMap[$from] as $trustItem) {
        $trust += trust($trustItem['id'], $to, $visited, $level + 1) * ($trustItem['rate'] / 100.0);
    }

    return $trust;
}

function info()
{
    $keys = KeyPair::getFromFiles(KEY_PATH);
    echo "Wallet: " . $keys->getWallet() . "\n";
}