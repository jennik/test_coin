<?php
require_once 'src/autoloader.php';

$keys = KeyPair::generateKeyPair(KEY_PATH);
$repo = new Repository();
$gm = new GossipMonger($repo);
$nonce = 0;

while (1) {
    $nonce++;
    $lastBlock = $repo->getLastBlock();
    $transactions = $repo->findLast10TransactionInPool();
    if ($lastBlock === null) {
        $block = Block::createGenesis($keys, $transactions, $nonce);
    } else {
        $block = Block::createNew($lastBlock, $keys, $transactions, $nonce);
    }

    $n = 4;
    if (substr($block->getHash(), -$n) === str_repeat('0', $n)) {
        $repo->addNewBlock($block);
        $nonce = 0;
        echo "FOUND: " . $block->getHash() . "\n";
        $gm->tellAboutNewBlock($block);

    } else {
        // echo "no: " . $block->getHash() . PHP_EOL;
    }
}
