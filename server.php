<?php
require_once 'src/autoloader.php';

$socket = stream_socket_server(
	'tcp://0.0.0.0:8333',
	$errno,
	$errstr
);

if (!$socket) {
	echo 'Cant create socket';
	die;
}

$repo = new Repository();
$gm = new GossipMonger($repo);



while ($conn = stream_socket_accept($socket, 100000000000)) {
//    var_dump(stream_socket_get_name($conn, true));die;
    $cmd = trim(fgets($conn));
    echo $cmd . PHP_EOL;
    switch ($cmd) {
        case 'new_addr':
            newAddr($conn);
            break;
        case 'get_addr':
            getAddr($conn);
            break;


        case 'get_transaction':
            getTransaction($conn);
            break;
        case 'new_transaction':
            newTransaction($conn);
            break;
        case 'transaction_list':
            transactionList($conn);
            break;


        case 'get_block':
            getBlock($conn);
            break;
        case 'new_block':
            newBlock($conn);
            break;


    }
	fclose($conn);
}


function newAddr($conn)
{
    global $repo;
    global $gm;
    $ips = [];
    $count = intval(fgets($conn));
    for ($i = 0; $i < $count; $i++) {
        $entry = explode(' ', fgets($conn));
        if (count($entry) == 2) {
            $ips[] = [
                'addr' => $entry[0],
                'ts' => $entry[1],
            ];
        }
    }

    $repo->addIps($ips);
    //send to friends
}

function getAddr($conn)
{
    global $repo;
    $ips = $repo->getIps();
    fputs($conn, count($ips) . "\n");
    foreach ($ips as $ip) {
        fputs($conn, "${ip['ip']} ${ip['timestamp']}\n");
    }
}

function getTransaction($conn)
{
    global $repo;
    $txid = trim(fgets($conn));
    $tr = (string) $repo->findTransactionById($txid);
    fputs($conn, strlen($tr) . "\n");
    if ($tr) {
        fputs($conn, "$tr\n");
    }
}

function newTransaction($conn)
{
    global $repo;
    global $gm;

    $len = intval(fgets($conn));
    $trStr = fread($conn, $len);

    $tr = Transaction::createNewFromString($trStr);

    if (!$tr) {
        return;
    }
    
    if (!$repo->findTransactionById($tr->getId())) {
        $repo->addNewTransaction($tr);
        $gm->tellAboutNewTransaction($tr);
    }
}

function transactionList($conn)
{
    global $repo;

    $trs = $repo->findAllTransactionInPool();

    fputs($conn, count($trs) . "\n");

    foreach ($trs as $tr) {
        fputs($conn, $tr->getId() . "\n");
    }
}

function getBlock($conn)
{
    global $repo;

    $blockId = trim(fgets($conn));
    $block = $repo->findBlockById($blockId);

    $block = (string) $block;
    $len = strlen($block);

    fputs($conn, $len . "\n");
    if ($block) {
        fputs($conn, $block . "\n");
    }
}



function newBlock($conn)
{
    global $repo;
    global $gm;

    $ip = stream_socket_get_name($conn, true);

    $len = intval(fgets($conn));
    $blockStr = fread($conn, $len);

    $block = Block::createNewFromString($blockStr);
    
    if (!$block) {
        return;
    }

    if ($block->getHash() !== $block->hashBlock()) {
        return;
    }

    $lastBlock = $repo->getLastBlock();
    if (!$lastBlock) {
        if ($block->getN() == 1) {
            $repo->addNewBlock($block);
        } else {
            $res = saveChain($block->getPrevHash(), $ip);
            if (!$res) {
                return;
            }
            $repo->addNewBlock($block);
        }
    } elseif ($lastBlock->getN() < $block->getN()) {
        if ($block->getPrevHash() === $lastBlock->getHash()) {
            $repo->addNewBlock($block);
        } else {
            $res = saveChain($block->getPrevHash(), $ip);
            if (!$res) {
                return;
            }
            $repo->addNewBlock($block);
        }
    } else {
        return;
    }
    
    
    $gm->tellAboutNewBlock($block);
}


function saveChain($hash, $ip)
{
    global $repo;
    $blockStr = getBlockById($hash, $ip);

    $block = Block::createNewFromString($blockStr);

    if (!$block) {
        return null;
    }

    if ($block->getHash() != $hash) {
        return null;
    }

    if ($block->getHash() !== $block->hashBlock()) {
        return null;
    }

    if ($block->getN() == 1) {
        $repo->addNewBlock($block);
        return $block;
    }

    $lastBlock = $repo->getLastBlock();
    if (!$lastBlock) {
        $lastBlock = saveChain($block->getPrevHash(), $ip);
        if (!$lastBlock) {
            return null;
        }
    }
    

    if ($block->getPrevHash() === $lastBlock->getHash()) {
        $repo->addNewBlock($block);
        return $block;
    } else {
        $retBlock = saveChain($block->getPrevHash(), $ip);
        if (!$retBlock) {
            return null;
        }

        $repo->addNewBlock($block);
        return $block;
    }
}

function getBlockById($hash, $ip)
{
    $hostParts = explode(':', $ip);
    $ip = $hostParts[0];
    $port = 8333;

    $conn = fsockopen($ip, $port, $errno, $errstr, 20);

    if (!$conn) {
        return '';
    }

    fwrite($conn, "get_block\n$hash\n");

    $len = intval(fgets($conn));

    $blockStr = fread($conn, $len);

    fclose($conn);
    return $blockStr;
}
