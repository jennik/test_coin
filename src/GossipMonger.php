<?php

class GossipMonger
{
    /**
     * @var Repository
     */
    private $repo;

    public function __construct(Repository $repo)
    {
        $this->repo = $repo;
    }

    public function tellAboutNewTransaction(Transaction $transaction)
    {
        $trStr = (string) $transaction;
        $len = strlen($trStr);
        $ips = $this->repo->getIps();
        foreach ($ips as $ip) {
            $conn = @fsockopen($ip['ip'], 8333, $errno, $errstr, 5);
            if (!$conn) {
                continue;
            }
            fwrite($conn, "new_transaction\n$len\n$trStr\n");
            fclose($conn);
        }
    }

    public function tellAboutNewBlock(Block $block)
    {
        $blStr = (string) $block;
        $len = strlen($blStr);
        $ips = $this->repo->getIps();
        foreach ($ips as $ip) {
            $conn = @fsockopen($ip['ip'],8333, $errno, $errstr, 5);
            if (!$conn) {
                continue;
            }
            fwrite($conn, "new_block\n$len\n$blStr\n");
            fclose($conn);
        }
    }
}