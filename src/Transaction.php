<?php

class Transaction implements SignableInterface
{
    private $from;
    private $to;
    private $amount;
    private $time;
    private $pubKey;
    private $signature;
    private $id;

    public function __call($name, $args)
    {
        if (strpos($name, 'get') === 0) {
            $name = lcfirst(substr($name, 3));
            return $this->{$name} ?? null;
        }

        if (strpos($name, 'set') === 0) {
            $name = lcfirst(substr($name, 3));
            $this->{$name} = $args[0];
        }
    }

    public static function createNewWithKeys($to, $amount, KeyPair $keys)
    {
        $tx = new static();
        $tx->to = $to;
        $tx->amount = $amount;
        $tx->from = $keys->getWallet();
        $tx->time = date('r');
        $tx->pubKey = $keys->getPublic();

        $tx->generateId();

        $tx->sign($keys);

        return $tx;
    }

    public static function createNewFromString($string)
    {
        $data = unserialize($string);

        $tx = new static();
        $tx->to = $data['to'];
        $tx->amount = $data['amount'];
        $tx->from = $data['from'];
        $tx->time = $data['time'];
        $tx->pubKey = $data['pubkey'];
        $tx->id = $data['id'];
        $tx->signature = $data['signature'];

        if ($tx->verify()) {
            return $tx;
        }
        return null;
    }

    public function generateId()
    {
        $this->id = hash('sha256', serialize([
            'from' => $this->from,
            'to' => $this->to,
            'amount' => $this->amount,
            'time' => $this->time,
            'pubkey' => $this->pubKey,
        ]));
    }

    public function sign(KeyPair $keys)
    {
        $this->signature = $keys->sign($this->dataForSign());
    }

    public function verify()
    {
        $signature = hex2bin($this->signature);
        $data = serialize($this->dataForSign());

        return 1 === openssl_verify($data, $signature, $this->pubKey, OPENSSL_ALGO_SHA256); 
    }

    public function dataForSign()
    {
        return [
            'id' => $this->id,
            'from' => $this->from,
            'to' => $this->to,
            'amount' => $this->amount,
            'time' => $this->time,
            'pubkey' => $this->pubKey,
        ];
    }

    public function __toString()
    {
        return serialize([
            'id' => $this->id,
            'from' => $this->from,
            'to' => $this->to,
            'amount' => $this->amount,
            'time' => $this->time,
            'pubkey' => $this->pubKey,
            'signature' => $this->signature,
        ]);
    }
}