<?php
const PROJ_ROOT = __DIR__ . '/../';
const KEY_PATH = PROJ_ROOT . 'data/';

spl_autoload_register(function ($className) {
    include_once __DIR__ . '/' . $className . '.php';
}, false);