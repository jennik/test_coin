<?php

class Block implements SignableInterface
{
    private $n;
    private $hash;
    private $prevHash = '';
    private $miner;
    private $pubKey;
    private $time;
    private $transactions = [];
    private $nonce;
    private $signature;
    
    public function __call($name, $args)
    {
        if (strpos($name, 'get') === 0) {
            $name = lcfirst(substr($name, 3));
            return $this->{$name} ?? null;
        }

        if (strpos($name, 'set') === 0) {
            $name = lcfirst(substr($name, 3));
            $this->{$name} = $args[0];
        }
    }

    public static function createNew(Block $prev, KeyPair $keys, array $transactions, $nonce)
    {
        $block = new static();
        $block->n = $prev->getN() + 1;
        $block->prevHash = $prev->getHash();
        $block->miner = $keys->getWallet();
        $block->pubKey = $keys->getPublic();
        $block->time = date('r');
        $block->transactions = $transactions;
        $block->nonce = $nonce;

        $block->generateHash();
        $block->signature = $keys->sign($block->dataForSign());
        
        return $block;
    }

    public static function createNewFromString($string)
    {
        $data = unserialize($string);

        $block = new static();
        $block->n = intval($data['n']);
        $block->prevHash = $data['prevHash'];
        $block->hash = $data['hash'];
        $block->miner = $data['miner'];
        $block->pubKey = $data['pubKey'];
        $block->time = $data['time'];
        $block->nonce = intval($data['nonce']);
        $block->signature = $data['signature'];

        
        $transactions = [];
        foreach ($data['transactions'] as $trStr) {
            $tr = Transaction::createNewFromString($trStr);
            if (!$tr) {
                return null;
            }
            if (!$tr->verify()) {
                return null;
            }

            $transactions[] = $tr;
        }
        
        $block->transactions = $transactions;
        
        if (!$block->verify()) {
            return null;
        }
        
        return $block;
    }

    public static function createGenesis(KeyPair $keys, array $transactions, $nonce)
    {
        $block = new static();
        $block->n = 1;
        $block->prevHash = '';
        $block->miner = $keys->getWallet();
        $block->pubKey = $keys->getPublic();
        $block->time = date('r');
        $block->transactions = $transactions;
        $block->nonce = $nonce;

        $block->generateHash();
        $block->sign($keys);

        return $block;
    }

    public function sign(KeyPair $keys)
    {
        $this->signature = $keys->sign($this->dataForSign());
    }

    public function verify()
    {
        $signature = hex2bin($this->signature);
        $data = serialize($this->dataForSign());

        foreach ($this->transactions as $tr) {
            if (!$tr->verify()) {
                return false;
            }
        }

        return 1 === openssl_verify($data, $signature, $this->pubKey, OPENSSL_ALGO_SHA256); 
    }

    public function generateHash()
    {
        $data = $this->dataForSign();
        unset($data['hash']);
        $this->hash = hash('sha256', serialize($data));
    }

    public function hashBlock()
    {
        $data = $this->dataForSign();
        unset($data['hash']);
        return hash('sha256', serialize($data));
    }

    public function dataForSign()
    {
        $transactions = [];

        foreach ($this->transactions as $tr) {
            $transactions[] = (string) $tr;
        }

        $data = [
            'n' => $this->n,
            'prevHash' => $this->prevHash,
            'hash' => $this->hash,
            'miner' => $this->miner,
            'pubKey' => $this->pubKey,
            'time' => $this->time,
            'transactions' => $transactions,
            'nonce' => $this->nonce,
        ];

        return $data;
    }

    public function __toString()
    {
        $transactions = [];

        foreach ($this->transactions as $tr) {
            $transactions[] = (string) $tr;
        }

        $data = [
            'n' => $this->n,
            'prevHash' => $this->prevHash,
            'hash' => $this->hash,
            'miner' => $this->miner,
            'pubKey' => $this->pubKey,
            'time' => $this->time,
            'transactions' => $transactions,
            'nonce' => $this->nonce,
            'signature' => $this->signature,
        ];

        return serialize($data);
    }

    
}