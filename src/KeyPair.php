<?php

class KeyPair 
{
    private $pub;
    private $priv;

    private function __construct($pub, $priv)
    {
        $this->pub = $pub;
        $this->priv = $priv;
    }

    public static function generateKeyPair($pathToSave = '')
    {
        $config = [
            'private_key_type' => OPENSSL_KEYTYPE_EC,
            'curve_name' => 'secp256k1',
            'private_key_bits' => 384,
        ];

        $res = openssl_pkey_new($config);

        openssl_pkey_export($res, $privateKey);

        $publicKey = openssl_pkey_get_details($res)['key'];

        if ($pathToSave) {
            file_put_contents($pathToSave . 'pub.key', $publicKey);
            file_put_contents($pathToSave . 'priv.key', $privateKey);
        }

        return new static($publicKey, $privateKey);
    }

    public static function getFromFiles($path)
    {
        return new static(
            file_get_contents($path . 'pub.key'),
            file_get_contents($path . 'priv.key')
        );
    }

    public function getPublic()
    {
        return $this->pub;
    }

    public function getPrivate()
    {
        return $this->priv;
    }

    public function getWallet()
    {
        return hash(
            'ripemd160',
            hash('sha256', $this->pub)
        );
    }

    public function sign($data)
    {
        if (is_array($data)) {
            $data = serialize($data);
        }

        openssl_sign($data, $signature, $this->priv, OPENSSL_ALGO_SHA256);

        return bin2hex($signature);
    }

    public function verify($data, $signature)
    {
        $signature = hex2bin($signature);

        if (is_array($data)) {
            $data = serialize($data);
        }

        return 1 === openssl_verify($data, $signature, $this->pub, OPENSSL_ALGO_SHA256);
    }


}