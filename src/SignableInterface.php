<?php

interface SignableInterface
{
    public function dataForSign();
}