<?php

class Repository
{
    private $db;
    public function __construct()
    {
        $this->db = new PDO('sqlite:' . PROJ_ROOT . 'data/blockchain.sqlte3');
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->db->setAttribute(PDO::ATTR_TIMEOUT, 120);
    }

    public function addNewTransaction(Transaction $tr)
    {
        if (!$tr->verify()) {
            return false;
        }

        $stmt = $this->db->prepare('INSERT OR IGNORE INTO transaction_pool (
            txid,
            "from",
            "to",
            amount,
            time,
            signature,
            pubKey
        ) VALUES (
            :txid,
            :from,
            :to,
            :amount,
            :time,
            :signature,
            :pubKey
        )');

        $stmt->bindValue('txid', $tr->getId());
        $stmt->bindValue('from', $tr->getFrom());
        $stmt->bindValue('to', $tr->getTo());
        $stmt->bindValue('amount', $tr->getAmount());
        $stmt->bindValue('time', $tr->getTime());
        $stmt->bindValue('signature', $tr->getSignature());
        $stmt->bindValue('pubKey', $tr->getPubKey());

        $res = $stmt->execute();
        $stmt->closeCursor();
        return $res;
    }

    public function findLast10TransactionInPool()
    {
        static $stmt = null;
        if ($stmt === null) {
            $stmt = $this->db->prepare('SELECT * FROM transaction_pool LIMIT 10');
        }

        $stmt->execute();

        $res = $stmt->fetchAll();

        $transactions = [];

        foreach ($res as $trData) {
            $tr = new Transaction();
            $tr->setFrom($trData['from']);
            $tr->setTo($trData['to']);
            $tr->setAmount($trData['amount']);
            $tr->setTime($trData['time']);
            $tr->setPubKey($trData['pubKey']);
            $tr->setSignature($trData['signature']);
            $tr->setId($trData['txid']);

            $transactions[] = $tr;
        }

        $stmt->closeCursor();
        return $transactions;
    }

    public function getLastBlock()
    {
        static $stmt = null;
        if ($stmt === null) {
            $stmt = $this->db->prepare('SELECT * FROM blocks ORDER BY n DESC LIMIT 1');
        }

        $stmt->execute();

        $res = $stmt->fetch();

        if (!$res) {
            return null;
        }

        $block = new Block();
        $block->setN(intval($res['n']));
        $block->setHash($res['hash']);
        $block->setPrevHash($res['prevHash']);
        $block->setMiner($res['miner']);
        $block->setPubKey($res['pubKey']);
        $block->setTime($res['time']);
        $block->setNonce(intval($res['nonce']));
        $block->setSignature($res['signature']);

        $trx = unserialize($res['transactions']);
        $transactions = [];
        foreach ($trx as $tr) {
            $transaction = new Transaction();
            $transaction->setFrom($tr['from']);
            $transaction->setTo($tr['to']);
            $transaction->setAmount($tr['amount']);
            $transaction->setTime($tr['time']);
            $transaction->setPubKey($tr['pubKey']);
            $transaction->setSignature($tr['signature']);
            $transaction->setId($tr['id']);

            $transactions[] = $transaction;
        }

        $block->setTransactions($transactions);

        $stmt->closeCursor();
        return $block;
    }

    public function addNewBlock($block)
    {
        if (!$block->verify())
        {
            return false;
        }

        static $stmt = null;
        if ($stmt === null) {
            $stmt = $this->db->prepare('INSERT INTO blocks (
                n,
                "hash",
                prevHash,
                miner,
                pubKey,
                "time",
                transactions,
                nonce,
                signature
            ) VALUES (
                :n,
                :hash,
                :prevHash,
                :miner,
                :pubKey,
                :time,
                :transactions,
                :nonce,
                :signature
            )');
        }

        $transactions = [];

        foreach($block->getTransactions() as $tr) {
            $transaction = [
                'from' => $tr->getFrom(),
                'to' => $tr->getTo(),
                'amount' => $tr->getAmount(),
                'time' => $tr->getTime(),
                'pubKey' => $tr->getPubKey(),
                'signature' => $tr->getSignature(),
                'id' => $tr->getId()
            ];
            $transactions[] = $transaction;
            $this->deleteTransactionFromPool($tr->getId());
        }

        $stmt->bindValue('n', $block->getN());
        $stmt->bindValue('hash', $block->getHash());
        $stmt->bindValue('prevHash', $block->getPrevHash());
        $stmt->bindValue('miner', $block->getMiner());
        $stmt->bindValue('pubKey', $block->getPubKey());
        $stmt->bindValue('time', $block->getTime());
        $stmt->bindValue('transactions', serialize($transactions));
        $stmt->bindValue('nonce', $block->getNonce());
        $stmt->bindValue('signature', $block->getSignature());

        $stmt->execute();
        $stmt->closeCursor();
    }

    public function deleteTransactionFromPool($id)
    {
        static $stmt = null;
        if ($stmt === null) {
            $stmt = $this->db->prepare('DELETE FROM transaction_pool WHERE txid = :id');
        }

        $stmt->bindValue('id', $id);
        $stmt->execute();
    }

    public function addIps(array $ips)
    {
        static $stmt = null;
        if ($stmt === null) {
            $stmt = $this->db->prepare('INSERT OR IGNORE INTO known_hosts (
                    ip, 
                    timestamp
                ) VALUES (
                    :ip,
                    :ts
                )');
        }

        foreach($ips as $ip) {
            $stmt->bindValue('ip', $ip['addr']);
            $stmt->bindParam('ts', $ip['ts']);
            $stmt->execute();
        }
    }

    public function getIps()
    {
        static $stmt = null;
        if ($stmt === null) {
            $stmt = $this->db->prepare('SELECT * FROM known_hosts');
        }

        $stmt->execute();
        $res = $stmt->fetchAll();
        $stmt->closeCursor();
        return $res;
    }

    public function findTransactionById($txid)
    {
        static $stmt = null;
        if ($stmt === null) {
            $stmt = $this->db->prepare('SELECT * FROM transaction_pool WHERE txid = :id');
        }

        $stmt->bindValue('id', $txid);

        $stmt->execute();

        $tr = $stmt->fetch();
        if (!$tr) {
            return null;
        }

        $transaction = new Transaction();
        $transaction->setFrom($tr['from']);
        $transaction->setTo($tr['to']);
        $transaction->setAmount($tr['amount']);
        $transaction->setTime($tr['time']);
        $transaction->setPubKey($tr['pubKey']);
        $transaction->setSignature($tr['signature']);
        $transaction->setId($tr['txid']);

        $stmt->closeCursor();
        return $transaction;
    }

    public function findAllTransactionInPool()
    {
        static $stmt = null;
        if ($stmt === null) {
            $stmt = $this->db->prepare('SELECT * FROM transaction_pool');
        }

        $stmt->execute();

        $res = $stmt->fetchAll();

        $transactions = [];

        foreach ($res as $trData) {
            $tr = new Transaction();
            $tr->setFrom($trData['from']);
            $tr->setTo($trData['to']);
            $tr->setAmount($trData['amount']);
            $tr->setTime($trData['time']);
            $tr->setPubKey($trData['pubKey']);
            $tr->setSignature($trData['signature']);
            $tr->setId($trData['txid']);

            $transactions[] = $tr;
        }

        $stmt->closeCursor(); 
        return $transactions;
    }

    public function findBlockById($blockHash)
    {
        static $stmt = null;
        if ($stmt === null) {
            $stmt = $this->db->prepare('SELECT * FROM blocks WHERE hash = :id');
        }

        $stmt->bindValue('id', $blockHash);

        $stmt->execute();

        $res = $stmt->fetch();

        if (!$res) {
            return null;
        }

        $block = new Block();
        $block->setN(intval($res['n']));
        $block->setHash($res['hash']);
        $block->setPrevHash($res['prevHash']);
        $block->setMiner($res['miner']);
        $block->setPubKey($res['pubKey']);
        $block->setTime($res['time']);
        $block->setNonce(intval($res['nonce']));
        $block->setSignature($res['signature']);

        $trx = unserialize($res['transactions']);
        $transactions = [];
        foreach ($trx as $tr) {
            $transaction = new Transaction();
            $transaction->setFrom($tr['from']);
            $transaction->setTo($tr['to']);
            $transaction->setAmount($tr['amount']);
            $transaction->setTime($tr['time']);
            $transaction->setPubKey($tr['pubKey']);
            $transaction->setSignature($tr['signature']);
            $transaction->setId($tr['id']);

            $transactions[] = $transaction;
        }

        $block->setTransactions($transactions);
        $stmt->closeCursor();
        return $block;
    }

    public function findAllTransactions()
    {
        static $res = [];
        if (!$res) {
            $stmt = $this->db->prepare("SELECT hash FROM blocks WHERE transactions != 'a:0:{}'");
            $stmt->execute();
            $hashes = $stmt->fetchAll();
            $stmt->closeCursor();

            foreach ($hashes as $hash) {
                $block = $this->findBlockById($hash['hash']);
                foreach ($block->getTransactions() as $transaction) {

                    if (!isset($res[$transaction->getFrom()])) {
                        $res[$transaction->getFrom()] = [
                            [
                                'id' => $transaction->getTo(),
                                'rate' => $transaction->getAmount(),
                            ]
                        ];
                    } else {
                        $res[$transaction->getFrom()][] = [
                            'id' => $transaction->getTo(),
                            'rate' => $transaction->getAmount(),
                        ];
                    }
                }
            }
        }

        return $res;
    }
}